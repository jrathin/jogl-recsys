import os

from jogl.recsys.config import Config

dir_path = os.path.dirname(os.path.realpath(__file__))
config_path = os.path.join(dir_path, 'test_config.yml')


def test_config():
    config = Config(config_path)
    assert len(config) == 2
    for (name, reco) in config.get_recos():
        assert 'source' in reco
        assert 'target' in reco
        assert 'top_k' in reco
        assert 'base_relation' in reco
        assert 'new_relation' in reco
        assert 'seen_relation' in reco
        assert 'meta_paths' in reco

        meta_paths = reco['meta_paths']
        assert len(meta_paths) == 2
        for mp in meta_paths:
            assert 'weight' in mp
            assert 'path' in mp
            assert len(mp['path']) == 2

        if name == 'user_community':
            assert reco['source'] == 'User'
            assert reco['target'] == 'Community'
            assert reco['top_k'] == 3
            assert reco['base_relation'] == 'User_has_followed_Community'
            assert reco['new_relation'] == 'UserCommunityRecommendations'
            assert reco['seen_relation'] == 'User_has_followed_Community'

            mp = meta_paths[0]

            assert mp['weight'] == 0.7
            assert mp['path'] == ['User_has_skill_Skill', 'inverse_Community_has_skill_Skill']
