import pandas as pd


class Store:
    """
    This is the base Store class for the recommender system, it will be used to be extended by different data stores.
    Each datastore will have at minima results and data to hold the datasets.
    Each datastore must extend the load_data and save_results methods.
    """

    def __init__(self):
        self.results = pd.DataFrame()
        self.data = None

    def preprocess_results(self):
        self.results.reset_index(inplace=True)
        del self.results['id']
        del self.results['index']
        self.results.index.rename('id', inplace=True)

    def add_results(self, df):
        self.results = self.results.append(df).reset_index(drop=True)
        self.results.index.rename('id', inplace=True)

    def load_data(self):
        '''
        Obtain data from the specified source and create the HIN object.
        '''
        raise("Please extend the load_data method in your custom store.")

    def save_results(self, *dfs):
        '''
        Save date in the given DataFrame to the given source.
        '''
        raise("Please extend the save_results method in your custom store.")
