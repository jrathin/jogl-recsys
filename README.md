# Recommender

Recommendation system for the JOGL platform.

Matchmaking algorithm for:

* (high priority) matching needs to resource based on

    * Tags (skills)
    * Geolocation
    * Network distance (how many steps away the individual is from me on JOGL)


* (2nd step) generating the feed on JOGL

Discussion ongoing on #prgm-metastudy-team-recommendation-system channel on Slack.

Need ton implement the heterogenous Information network method (https://github.com/pedroramaciotti/HINPy).

## Usage

### As a docker

We are using a docker-compose file to create the right environment as well as help with building images.

First build the docker images with:
`docker-compose build`

*Alternatively, you can use the makefile command ```shell make docker-install``` *

You should see an output that has the `Successfully` at the end

```
Building db

[...]

Successfully tagged jogl-recsys_db:latest
Building engine

[...]

Successfully tagged jogl-recsys_engine:latest
```

Then launch the stack with

`docker-compose up -d`

```
Creating jogl-recsys_db_1 ... done
Creating jogl-recsys_engine_1 ... done
```

You can then check for the execution state of the engine with

`docker-compose logs -f engine`

```
Attaching to jogl-recsys_engine_1
engine_1  | JOGL Recommender System - Learn more at https://jogl.io
engine_1  | Will use the JOGL Database as data source.
engine_1  | Will make top 3 community recommendations for all users
engine_1  | 2583 recommendations in 9.2s (279.5 recommendations/s)
engine_1  | Will make top 3 need recommendations for all users
engine_1  | 639 recommendations in 2.4s (264.4 recommendations/s)
engine_1  | Will make top 3 project recommendations for all users
engine_1  | 8637 recommendations in 33.4s (258.5 recommendations/s)
engine_1  | Total elapsed time: 54.3s
jogl-recsys_engine_1 exited with code 0
```

If you see a `jogl-recsys_engine_1 exited with code 0` Then all went well :D

### As a python module


Basic installation: 
Install the requirements with pip: `pipenv install -r requirements.txt`

*It is recommended to create a new environnement (e.g with pipenv) and install the project dependencies inside it.*
```shell
pipenv shell
pipenv install -r requirements.txt
```
You can then find the help with `python -m jogl.recsys --help`

```
Usage: jogl.recsys [OPTIONS] [CSV_FILE]

  JOGL Recommender System

  Accepts a --source flag that can specify obtaining data from the jogl
  network api, a csv file or the jogl database (default)

  CSV_FILE is the name of the file that will be used to load data the
  --source csv flag.  Recommendations are saved to the csv_prediction.csv file.

  Learn more at https://jogl.io

Options:
  -s, --source [api|csv|database]
                                  Sets the type of source to get data from.
  --help                          Show this message and exit.
```

#### *Installation issues on MacOSX*

*With the package Psycopg2:*

Sometimes, the package psycopg2 doesn't install properly because the library -lssl is not found. You can fix it with by installing the command lines tools:
```shell
xcode-select --install
pip install psycopg2``` 
or with pipenv
```shell
pipenv install psycopg2
``` 

if it doesn't works, try to link against brew's openssl: 
```env LDFLAGS="-I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib" pip install psycopg2```

More informations:
https://stackoverflow.com/questions/26288042/error-installing-psycopg2-library-not-found-for-lssl

### Database

TODO: Write more documentation

You will need to provide a `POSTGRES_URL` env variable of the classic format: `postgres://[username]:[password]@[URI]:[port]/[database_name]`, for example: `export POSTGRES_URL=postgres://jogl:password@db/jogl`.

To launch with the database as a source datastore, you can run `python -m jogl.recsys -s database`, or as database is the default store: `python -m jogl.recsys`.

To check if the results got written correctly, connect to the POSTGRES database through docker: `docker-compose exec db psql -U jogl`.  You can look for all the tables with `\dt`, and check that the results got written correctly with `select * from recsys_results;`

### Network

TODO: Write more documentation

To launch with the network from JOGL API as a source datastore, you can run `python -m jogl.recsys -s api`.

### CSV

TODO: Write more documentation

To launch with a CSV file as a source datastore, you can run `python -m jogl.recsys -s csv CSV_FILE`. You can test this with the example file provided in the `samples` folder: `python -m jogl.recsys -s csv jogl/recsys/samples/data.csv`

## Contributors

Many thanks to:

* [Luis Arias](https://gitlab.com/kaaloo1)
* [Pedro Ramaciotti](https://github.com/pedroramaciotti)
