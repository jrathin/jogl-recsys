import click
import sys

from jogl.recsys import RecommenderEngine
from jogl.recsys.config import Config
from jogl.recsys.stores import CSVStore, NetworkApiStore, DatabaseStore
from ttictoc import Timer

# TODO: figure out how to add help="Defines the path to the CSV file to be used as a datasource."
# to the csv_file so that it appears in the help menu !


@click.command(context_settings=dict(ignore_unknown_options=False))
@click.option('-c', '--config', type=click.Path(exists=True), required=True)
@click.option('-s', '--source', type=click.Choice(['api', 'csv', 'database']), default='database', help='Sets the type of source to get data from.')
@click.argument('csv_file', type=click.Path(exists=True), required=False)
def main(source, config, csv_file):
    '''
    JOGL Recommender System

    Accepts a --source flag that can specify obtaining data from
    the jogl network api, a csv file or the jogl database (default)

    CSV_FILE is the name of the file that will be used to load data
    the --source csv flag.  Recommendations are printed to stdout.

    Learn more at https://jogl.io
    '''

    click.echo('JOGL Recommender System - Learn more at https://jogl.io')

    with Timer(verbose=False) as T:
        # This is the store selection switch
        # allowing for multiple type of data to be used
        # This will make it easy for data science folks to test
        # and implement new path locally.
        if source == 'api':
            click.echo('Will use the JOGL Network Api as data source.')
            store = NetworkApiStore()
        elif source == 'csv':
            if not csv_file:
                raise("CSV file is missing!")
            click.echo(
                'Will use the {} csv file as data source.'.format(csv_file))
            store = CSVStore(csv_file)
        elif source == 'database':
            click.echo('Will use the JOGL Database as data source.')
            store = DatabaseStore()
        else:
            click.echo('Unknown data source: {}'.format(source))
            sys.exit(2)

        click.echo('Using config file {}'.format(config))
        config = Config(config)

        engine = RecommenderEngine(store, config)
        engine.make_all_recos()

        store.save_results()

    click.echo('Total elapsed time: {:.1f}s'.format(T.elapsed))


if __name__ == '__main__':
    # pylint: disable=no-value-for-parameter
    main()
