CREATE TABLE recsys_data (
  id INT NOT NULL,
  sourceable_node_type VARCHAR(1000),
  sourceable_node_id INT,
  targetable_node_type VARCHAR(1000),
  targetable_node_id INT,
  relation_type VARCHAR(1000),
  value FLOAT DEFAULT 0.0,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE recsys_results (
  id INT NOT NULL,
  sourceable_node_type VARCHAR(1000),
  sourceable_node_id INT,
  targetable_node_type VARCHAR(1000),
  targetable_node_id INT,
  relation_type VARCHAR(1000),
  value FLOAT,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

\copy recsys_data FROM '/data/recsys_datum_export.csv' DELIMITER ',' CSV HEADER;
